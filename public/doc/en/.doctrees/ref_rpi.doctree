���]      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Gpiozero (Raspberry Pi) docs�h]�h	�Text����Gpiozero (Raspberry Pi) docs�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�./home/matyas/Projects/turtlico/doc/ref_rpi.rst�hKubh	�	container���)��}�(hX�  **Connect LED**

Returns an object to control the part.
You can use `on <#rpi-on>`_ and `off <#rpi-off>`_ commands to control it.
To get or set current state use the `value <#rpi-value>`_ property.
To determine which pin is the part connected into you can see `the pin numbering scheme <https://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering_>`.

*Parameters*

* ``NUMBER`` Pin where the LED is physically connected�h]�(h	�	paragraph���)��}�(h�**Connect LED**�h]�h	�strong���)��}�(hh6h]�h�Connect LED�����}�(hhhh:ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hh4ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhh.ubh3)��}�(hXV  Returns an object to control the part.
You can use `on <#rpi-on>`_ and `off <#rpi-off>`_ commands to control it.
To get or set current state use the `value <#rpi-value>`_ property.
To determine which pin is the part connected into you can see `the pin numbering scheme <https://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering_>`.�h]�(h�3Returns an object to control the part.
You can use �����}�(h�3Returns an object to control the part.
You can use �hhMubh	�	reference���)��}�(h�`on <#rpi-on>`_�h]�h�on�����}�(h�on�hhXubah}�(h ]�h"]�h$]�h&]�h(]��name�h`�refuri��#rpi-on�uh*hVhhMubh	�target���)��}�(h�
 <#rpi-on>�h]�h}�(h ]��on�ah"]�h$]��on�ah&]�h(]��refuri�hiuh*hj�
referenced�KhhMubh� and �����}�(h� and �hhMubhW)��}�(h�`off <#rpi-off>`_�h]�h�off�����}�(h�off�hhubah}�(h ]�h"]�h$]�h&]�h(]��name�h�hh�#rpi-off�uh*hVhhMubhk)��}�(h� <#rpi-off>�h]�h}�(h ]��off�ah"]�h$]��off�ah&]�h(]��refuri�h�uh*hjhyKhhMubh�= commands to control it.
To get or set current state use the �����}�(h�= commands to control it.
To get or set current state use the �hhMubhW)��}�(h�`value <#rpi-value>`_�h]�h�value�����}�(h�value�hh�ubah}�(h ]�h"]�h$]�h&]�h(]��name�h�hh�
#rpi-value�uh*hVhhMubhk)��}�(h� <#rpi-value>�h]�h}�(h ]��value�ah"]�h$]��value�ah&]�h(]��refuri�h�uh*hjhyKhhMubh�I property.
To determine which pin is the part connected into you can see �����}�(h�I property.
To determine which pin is the part connected into you can see �hhMubh	�title_reference���)��}�(h�b`the pin numbering scheme <https://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering_>`�h]�h�`the pin numbering scheme <https://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering_>�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hhMubh�.�����}�(h�.�hhMubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK	hh.ubh3)��}�(h�*Parameters*�h]�h	�emphasis���)��}�(hh�h]�h�
Parameters�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhh.ubh	�bullet_list���)��}�(hhh]�h	�	list_item���)��}�(h�4``NUMBER`` Pin where the LED is physically connected�h]�h3)��}�(hj  h]�(h	�literal���)��}�(h�
``NUMBER``�h]�h�NUMBER�����}�(hhhj	  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj  ubh�* Pin where the LED is physically connected�����}�(h�* Pin where the LED is physically connected�hj  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhj   ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hh�ubah}�(h ]�h"]�h$]�h&]�h(]��bullet��*�uh*h�hh+hKhh.ubeh}�(h ]��rpi-led�ah"]��cmd�ah$]��rpi-led�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(hX{  **Connect RGB LED**

Returns an object to control the part. Use `value <#rpi-value>`_ and `RPi color <#rpi-color>`_ to control the color of the LED.

*Parameters*

* ``NUMBER`` "red" Pin that controls the red component of the RGB LED
* ``NUMBER`` "green" Pin that controls the green component of the RGB LED
* ``NUMBER`` "blue" Pin that controls the blue component of the RGB LED�h]�(h3)��}�(h�**Connect RGB LED**�h]�h9)��}�(hj?  h]�h�Connect RGB LED�����}�(hhhjA  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj=  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhj9  ubh3)��}�(h�Returns an object to control the part. Use `value <#rpi-value>`_ and `RPi color <#rpi-color>`_ to control the color of the LED.�h]�(h�+Returns an object to control the part. Use �����}�(h�+Returns an object to control the part. Use �hjT  ubhW)��}�(h�`value <#rpi-value>`_�h]�h�value�����}�(h�value�hj]  ubah}�(h ]�h"]�h$]�h&]�h(]��name�je  hh�
#rpi-value�uh*hVhjT  ubhk)��}�(h� <#rpi-value>�h]�h}�(h ]��id1�ah"]�h$]�h&]��value�ah(]��refuri�jm  uh*hjhyKhjT  ubh� and �����}�(h� and �hjT  ubhW)��}�(h�`RPi color <#rpi-color>`_�h]�h�	RPi color�����}�(h�	RPi color�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]��name��	RPi color�hh�
#rpi-color�uh*hVhjT  ubhk)��}�(h� <#rpi-color>�h]�h}�(h ]��	rpi-color�ah"]�h$]��	rpi color�ah&]�h(]��refuri�j�  uh*hjhyKhjT  ubh�! to control the color of the LED.�����}�(h�! to control the color of the LED.�hjT  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhj9  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hj�  h]�h�
Parameters�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhj9  ubh�)��}�(hhh]�(h�)��}�(h�C``NUMBER`` "red" Pin that controls the red component of the RGB LED�h]�h3)��}�(hj�  h]�(j  )��}�(h�
``NUMBER``�h]�h�NUMBER�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj�  ubh�= “red” Pin that controls the red component of the RGB LED�����}�(h�9 "red" Pin that controls the red component of the RGB LED�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubh�)��}�(h�G``NUMBER`` "green" Pin that controls the green component of the RGB LED�h]�h3)��}�(hj�  h]�(j  )��}�(h�
``NUMBER``�h]�h�NUMBER�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj�  ubh�A “green” Pin that controls the green component of the RGB LED�����}�(h�= "green" Pin that controls the green component of the RGB LED�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubh�)��}�(h�E``NUMBER`` "blue" Pin that controls the blue component of the RGB LED�h]�h3)��}�(hj  h]�(j  )��}�(h�
``NUMBER``�h]�h�NUMBER�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj  ubh�? “blue” Pin that controls the blue component of the RGB LED�����}�(h�; "blue" Pin that controls the blue component of the RGB LED�hj  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hKhj9  ubeh}�(h ]��rpi-rgb�ah"]��cmd�ah$]��rpi-rgb�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(h��**Connect Button**

Returns an object check state of the part. The part can also be any digital input device.

*Parameters*

* ``NUMBER`` Pin where the Button or other digital input device is physically connected.�h]�(h3)��}�(h�**Connect Button**�h]�h9)��}�(hjK  h]�h�Connect Button�����}�(hhhjM  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hjI  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK!hjE  ubh3)��}�(h�YReturns an object check state of the part. The part can also be any digital input device.�h]�h�YReturns an object check state of the part. The part can also be any digital input device.�����}�(hjb  hj`  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK#hjE  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hjp  h]�h�
Parameters�����}�(hhhjr  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hjn  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK%hjE  ubh�)��}�(hhh]�h�)��}�(h�V``NUMBER`` Pin where the Button or other digital input device is physically connected.�h]�h3)��}�(hj�  h]�(j  )��}�(h�
``NUMBER``�h]�h�NUMBER�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj�  ubh�L Pin where the Button or other digital input device is physically connected.�����}�(h�L Pin where the Button or other digital input device is physically connected.�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK'hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hK'hjE  ubeh}�(h ]��rpi-btn�ah"]��cmd�ah$]��rpi-btn�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(hXl  **Connect PWM Output Device**

Returns an object to control a PWM output device.
PWM means that the `value <#rpi-value>`_ of the device can be anywhere in range from 0 to 1 (including decimal values).
The signal is then quickly switched on and off depending on the value. 0.0 is off and 1.0 is fully on.

*Parameters*

* ``NUMBER`` Pin where the part is connected.�h]�(h3)��}�(h�**Connect PWM Output Device**�h]�h9)��}�(hj�  h]�h�Connect PWM Output Device�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK+hj�  ubh3)��}�(hX  Returns an object to control a PWM output device.
PWM means that the `value <#rpi-value>`_ of the device can be anywhere in range from 0 to 1 (including decimal values).
The signal is then quickly switched on and off depending on the value. 0.0 is off and 1.0 is fully on.�h]�(h�EReturns an object to control a PWM output device.
PWM means that the �����}�(h�EReturns an object to control a PWM output device.
PWM means that the �hj�  ubhW)��}�(h�`value <#rpi-value>`_�h]�h�value�����}�(h�value�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]��name�j�  hh�
#rpi-value�uh*hVhj�  ubhk)��}�(h� <#rpi-value>�h]�h}�(h ]��id2�ah"]�h$]�h&]��value�ah(]��refuri�j�  uh*hjhyKhj�  ubh�� of the device can be anywhere in range from 0 to 1 (including decimal values).
The signal is then quickly switched on and off depending on the value. 0.0 is off and 1.0 is fully on.�����}�(h�� of the device can be anywhere in range from 0 to 1 (including decimal values).
The signal is then quickly switched on and off depending on the value. 0.0 is off and 1.0 is fully on.�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK-hj�  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hj  h]�h�
Parameters�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj
  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK1hj�  ubh�)��}�(hhh]�h�)��}�(h�+``NUMBER`` Pin where the part is connected.�h]�h3)��}�(hj&  h]�(j  )��}�(h�
``NUMBER``�h]�h�NUMBER�����}�(hhhj+  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj(  ubh�! Pin where the part is connected.�����}�(h�! Pin where the part is connected.�hj(  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK3hj$  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj!  ubah}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hK3hj�  ubeh}�(h ]��rpi-pwm�ah"]��cmd�ah$]��rpi-pwm�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(hX1  **Connect Digital Output Device**

Retruns an object to control a device that has two states: on and off
You can use `on <#rpi-on>`_ and `off <#rpi-off>`_ commands to control it
To get or set current state use the `value <#rpi-value>`_ property

*Parameters*

* ``NUMBER`` Pin where the part is connected.�h]�(h3)��}�(h�!**Connect Digital Output Device**�h]�h9)��}�(hj_  h]�h�Connect Digital Output Device�����}�(hhhja  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj]  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK7hjY  ubh3)��}�(h��Retruns an object to control a device that has two states: on and off
You can use `on <#rpi-on>`_ and `off <#rpi-off>`_ commands to control it
To get or set current state use the `value <#rpi-value>`_ property�h]�(h�RRetruns an object to control a device that has two states: on and off
You can use �����}�(h�RRetruns an object to control a device that has two states: on and off
You can use �hjt  ubhW)��}�(h�`on <#rpi-on>`_�h]�h�on�����}�(h�on�hj}  ubah}�(h ]�h"]�h$]�h&]�h(]��name�j�  hh�#rpi-on�uh*hVhjt  ubhk)��}�(h�
 <#rpi-on>�h]�h}�(h ]��id3�ah"]�h$]�h&]��on�ah(]��refuri�j�  uh*hjhyKhjt  ubh� and �����}�(h� and �hjt  ubhW)��}�(h�`off <#rpi-off>`_�h]�h�off�����}�(h�off�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]��name�j�  hh�#rpi-off�uh*hVhjt  ubhk)��}�(h� <#rpi-off>�h]�h}�(h ]��id4�ah"]�h$]�h&]��off�ah(]��refuri�j�  uh*hjhyKhjt  ubh�< commands to control it
To get or set current state use the �����}�(h�< commands to control it
To get or set current state use the �hjt  ubhW)��}�(h�`value <#rpi-value>`_�h]�h�value�����}�(h�value�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]��name�j�  hh�
#rpi-value�uh*hVhjt  ubhk)��}�(h� <#rpi-value>�h]�h}�(h ]��id5�ah"]�h$]�h&]��value�ah(]��refuri�j�  uh*hjhyKhjt  ubh�	 property�����}�(h�	 property�hjt  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK9hjY  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hj�  h]�h�
Parameters�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK=hjY  ubh�)��}�(hhh]�h�)��}�(h�+``NUMBER`` Pin where the part is connected.�h]�h3)��}�(hj  h]�(j  )��}�(h�
``NUMBER``�h]�h�NUMBER�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj
  ubh�! Pin where the part is connected.�����}�(h�! Pin where the part is connected.�hj
  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK?hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj  ubah}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hK?hjY  ubeh}�(h ]��rpi-out�ah"]��cmd�ah$]��rpi-out�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(h�/**Turn on device method**

*Parameters*

* None�h]�(h3)��}�(h�**Turn on device method**�h]�h9)��}�(hjA  h]�h�Turn on device method�����}�(hhhjC  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj?  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKChj;  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hjX  h]�h�
Parameters�����}�(hhhjZ  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hjV  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKEhj;  ubh�)��}�(hhh]�h�)��}�(h�None�h]�h3)��}�(hjr  h]�h�None�����}�(hjr  hjt  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKGhjp  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hjm  ubah}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hKGhj;  ubeh}�(h ]��rpi-on�ah"]��cmd�ah$]��rpi-on�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(h�0**Turn off device method**

*Parameters*

* None�h]�(h3)��}�(h�**Turn off device method**�h]�h9)��}�(hj�  h]�h�Turn off device method�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKKhj�  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hj�  h]�h�
Parameters�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKMhj�  ubh�)��}�(hhh]�h�)��}�(h�None�h]�h3)��}�(hj�  h]�h�None�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKOhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hKOhj�  ubeh}�(h ]��rpi-off�ah"]��cmd�ah$]��rpi-off�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(h��**Value property (input and output)**

You can get or set the state of a device via this property. See individual devices for details.�h]�(h3)��}�(h�%**Value property (input and output)**�h]�h9)��}�(hj�  h]�h�!Value property (input and output)�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKShj�  ubh3)��}�(h�_You can get or set the state of a device via this property. See individual devices for details.�h]�h�_You can get or set the state of a device via this property. See individual devices for details.�����}�(hj  hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKUhj�  ubeh}�(h ]��	rpi-value�ah"]��cmd�ah$]��	rpi-value�ah&]�h(]�uh*h,hhhhhh+hNubh-)��}�(hXZ  **Converts color to format suitable for use with gpiozero**

Gpiozero functions (i.e. value of LEDs) do not accept the normal colors represented by three values in range 0 - 255.
This functions converts the normal Turtlico color to the format accepted by gpiozero (three values in range 0.0 - 1.0).

*Parameters*

* ``COLOR`` The color to convert�h]�(h3)��}�(h�;**Converts color to format suitable for use with gpiozero**�h]�h9)��}�(hj)  h]�h�7Converts color to format suitable for use with gpiozero�����}�(hhhj+  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj'  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKZhj#  ubh3)��}�(h��Gpiozero functions (i.e. value of LEDs) do not accept the normal colors represented by three values in range 0 - 255.
This functions converts the normal Turtlico color to the format accepted by gpiozero (three values in range 0.0 - 1.0).�h]�h��Gpiozero functions (i.e. value of LEDs) do not accept the normal colors represented by three values in range 0 - 255.
This functions converts the normal Turtlico color to the format accepted by gpiozero (three values in range 0.0 - 1.0).�����}�(hj@  hj>  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK\hj#  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hjN  h]�h�
Parameters�����}�(hhhjP  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hjL  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hK_hj#  ubh�)��}�(hhh]�h�)��}�(h�``COLOR`` The color to convert�h]�h3)��}�(hjh  h]�(j  )��}�(h�	``COLOR``�h]�h�COLOR�����}�(hhhjm  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hjj  ubh� The color to convert�����}�(h� The color to convert�hjj  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKahjf  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hjc  ubah}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hKahj#  ubeh}�(h ]��id6�ah"]��cmd�ah$]��	rpi-color�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(h��**Connect a function to check when device is turned on**

*Parameters*

* ``DEVICE`` The device.
* ``FUNCTION`` Callback with no arguments�h]�(h3)��}�(h�8**Connect a function to check when device is turned on**�h]�h9)��}�(hj�  h]�h�4Connect a function to check when device is turned on�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKehj�  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hj�  h]�h�
Parameters�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKghj�  ubh�)��}�(hhh]�(h�)��}�(h�``DEVICE`` The device.�h]�h3)��}�(hj�  h]�(j  )��}�(h�
``DEVICE``�h]�h�DEVICE�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj�  ubh� The device.�����}�(h� The device.�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKihj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubh�)��}�(h�'``FUNCTION`` Callback with no arguments�h]�h3)��}�(hj�  h]�(j  )��}�(h�``FUNCTION``�h]�h�FUNCTION�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj�  ubh� Callback with no arguments�����}�(h� Callback with no arguments�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKjhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hKihj�  ubeh}�(h ]��rpi-event-on�ah"]��cmd�ah$]��rpi-event-on�ah&]�h(]�uh*h,hhhhhNhNubh-)��}�(h��**Connect a function to check when device is turned off**

*Parameters*

* ``DEVICE`` The device.
* ``FUNCTION`` Callback with no arguments�h]�(h3)��}�(h�9**Connect a function to check when device is turned off**�h]�h9)��}�(hj1  h]�h�5Connect a function to check when device is turned off�����}�(hhhj3  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h8hj/  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKnhj+  ubh3)��}�(h�*Parameters*�h]�h�)��}�(hjH  h]�h�
Parameters�����}�(hhhjJ  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hjF  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKphj+  ubh�)��}�(hhh]�(h�)��}�(h�``DEVICE`` The device.�h]�h3)��}�(hjb  h]�(j  )��}�(h�
``DEVICE``�h]�h�DEVICE�����}�(hhhjg  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hjd  ubh� The device.�����}�(h� The device.�hjd  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKrhj`  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj]  ubh�)��}�(h�'``FUNCTION`` Callback with no arguments�h]�h3)��}�(hj�  h]�(j  )��}�(h�``FUNCTION``�h]�h�FUNCTION�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj�  ubh� Callback with no arguments�����}�(h� Callback with no arguments�hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h2hh+hKshj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hj]  ubeh}�(h ]�h"]�h$]�h&]�h(]�j.  j/  uh*h�hh+hKrhj+  ubeh}�(h ]��rpi-event-off�ah"]��cmd�ah$]��rpi-event-off�ah&]�h(]�uh*h,hhhhhNhNubeh}�(h ]��gpiozero-raspberry-pi-docs�ah"]�h$]��gpiozero (raspberry pi) docs�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j6  j2  huhrh�h�h�h�jB  j>  j�  j�  j�  j�  jV  jR  j8  j4  j�  j�  j�  j�  j   j  j�  j�  j(  j$  j�  j�  u�	nametypes�}�(j�  Nj6  �hu�h��h��jB  �j�  �j�  �jV  �j8  �j�  �j�  �j   �j�  �j(  �j�  �uh }�(j�  hj2  h.hrhlh�h�h�h�j>  j9  jt  jn  j�  j�  j�  jE  jR  j�  j�  j�  j4  jY  j�  j�  j�  j�  j�  j�  j�  j;  j�  j�  j  j�  j�  j#  j$  j�  j�  j+  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]�(h	�system_message���)��}�(hhh]�h3)��}�(h�(Duplicate explicit target name: "value".�h]�h�,Duplicate explicit target name: “value”.�����}�(hhhjH  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hjE  ubah}�(h ]�h"]�h$]�h&]�h(]�jt  a�level�K�type��INFO��source�h+�line�Kuh*jC  hj9  ubjD  )��}�(hhh]�h3)��}�(h�(Duplicate explicit target name: "value".�h]�h�,Duplicate explicit target name: “value”.�����}�(hhhjd  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hja  ubah}�(h ]�h"]�h$]�h&]�h(]�j�  a�level�K�type�j^  �source�h+�line�Kuh*jC  hj�  ubjD  )��}�(hhh]�h3)��}�(h�%Duplicate explicit target name: "on".�h]�h�)Duplicate explicit target name: “on”.�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hj|  ubah}�(h ]�h"]�h$]�h&]�h(]�j�  a�level�K�type�j^  �source�h+�line�Kuh*jC  hjY  ubjD  )��}�(hhh]�h3)��}�(h�&Duplicate explicit target name: "off".�h]�h�*Duplicate explicit target name: “off”.�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�j�  a�level�K�type�j^  �source�h+�line�Kuh*jC  hjY  ubjD  )��}�(hhh]�h3)��}�(h�(Duplicate explicit target name: "value".�h]�h�,Duplicate explicit target name: “value”.�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h2hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�j�  a�level�K�type�j^  �source�h+�line�Kuh*jC  hjY  ube�transform_messages�]��transformer�N�
decoration�Nhhub.